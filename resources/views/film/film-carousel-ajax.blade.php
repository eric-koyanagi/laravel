<?php $first = true; ?>
<?php foreach($films as $film): ?>
    <div class="carousel-item <?php if($first) echo 'active' ?> text-center">
      <img src="{{ ($film->photo) }}" alt="Chania">
      
      <div class="carousel-caption">
        <h3><a href="<?php echo url('/film/' . $film->slug); ?>">{{$film->name}}</a></h3>
        <p>{{ $film->description }}</p>
      </div>
    </div>
      
    <?php $first = false; ?>
<?php endforeach; ?>