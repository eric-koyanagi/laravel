@extends('layouts.app')

@section('head')
<style>
    #film-form input { width:100% }
</style>    
@endsection

@section('content')   
    <div class="container centered">
        <div style="margin-bottom:1em">
            <a href="../films"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></a>
        </div>    
        <div id="film-form" class="card card-default">
            <div class="card-header"><span class="fa fa-youtube-play"></span> <strong>Create Film</strong></div>
            <div class="card-body row">
                <div class="p-3 w-100">
            
                @if(isset($film))
                {!! Form::model($film, ['route' => ['film/edit', $film], 'files' => true]) !!}
                @else
                {{ Form::open(['route' => 'film.create', 'files' => true]) }}
                @endif
                
                <div class="row">
                    <div class="col-sm-2 text-right">{{Form::label('name', 'Film Title')}}</div>
                    <div class="col-sm-4">{{Form::text('name', null, ['required'])}}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2 text-right">{{Form::label('description', 'Description')}}</div>
                    <div class="col-sm-4">{{Form::textarea('description', null, ['size' => '45x4'])}}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2 text-right">{{Form::label('release_date', 'Released On')}}</div>
                    <div class="col-sm-4">{{Form::date('release_date')}}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2 text-right">{{Form::label('rating', 'Rating')}}</div>
                    <div class="col-sm-4">{{Form::selectRange('rating', 1, 5)}}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2 text-right">{{Form::label('ticket_price', 'Ticket Price')}}</div>
                    <div class="col-sm-4">{{Form::number('ticket_price', null, ['step' => 0.5])}}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2 text-right">{{Form::label('country', 'Country')}}</div>
                    <div class="col-sm-4">{{Form::text('country')}}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2 text-right">{{Form::label('photo', 'Photo')}}</div>
                    <div class="col-sm-4">{{Form::file('photo')}}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2 text-right">{{Form::label('slug', 'URL Slug')}}</div>
                    <div class="col-sm-4">{{Form::text('slug', null, ['required'])}}</div>
                </div>
                
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4">{{Form::submit()}}</div>
                </div>
                    
                {!! Form::close() !!}                
                </div>
            </div>
        </div>

    </div>
        
@endsection