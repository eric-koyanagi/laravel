@extends('layouts.app')

@section('head')
<script>
$(function(){
    $('#comment-form').on('submit',function(e){        
        e.preventDefault(e);
        
        $.ajaxSetup({
            header:$('meta[name="_token"]').attr('content')
        })
        $.ajax({
            type:"POST",
            url:'./addComment',
            data:$(this).serialize(),
            success: function(data){
                window.location.reload();
            }
        })
    });
});
</script>
@endsection

@section('content')   
    <div class="container centered">
        <div style="margin-bottom:1em">
            <a href="../films"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></a>
        </div>    
        <div class="card card-default">
            <div class="card-header"><span class="fa fa-youtube-play"></span> <strong>Film Information</strong></div>
            <div class="card-body row">
                <div class="col-sm"><img src="{{$film->photo}}" alt="{{$film->name}}" class="w-100"/></div>
                <div class="col-sm">
                    <table class="table table-hover table-striped">
                        <tr><td><strong>Title</strong></td><td>{{$film->name}}</td></tr>
                        <tr><td><strong>Description</strong></td><td>{{$film->description}}</td></tr>
                        <tr><td><strong>Released</strong></td><td>{{ \Carbon\Carbon::parse($film->release_date)->format('d/m/Y')}}</td></tr>
                        <tr>
                            <td><strong>Rating</strong></td>
                            <td>
                            @for ($i = 0; $i < $film->rating; $i++)
                                <span class="fa fa-star-o"></span>
                            @endfor
                            </td>
                        </tr>
                            
                        <tr><td><strong>Ticket Price</strong></td><td>$<?php echo number_format($film->ticket_price, 2);?></td></tr>
                        <tr><td><strong>Country</strong></td><td>{{$film->country}}</td></tr>
                        <tr><td><strong>Genres</strong></td><td>{{implode(',', $film->genres->map(function ($arr) { return $arr->name; })->toArray())}}</td></tr>
                        
                    </table>
                </div>
                
            </div>
        </div>
        
        <div class="card card-default mt-3">
            <div class="card-header"><span class="fa fa-comment"></span> <strong>Comments</strong></div>
            <div class="card-body">

                @foreach ($film->comments as $comment)
                    <div class="row"><div class="col-sm-2 text-right"><strong>{{$comment->name}}:</strong></div><div class="col-lg pl-1">{{$comment->comment}}</div></div>
                @endforeach

            </div>
        </div>
            
        <div class="card card-default mt-3">
            <div class="card-header"><span class="fa fa-commenting"></span> <strong>Add a Comment</strong></div>
            <div class="card-body">
                @if(Auth::user())
                    {{ Form::open(['id' => 'comment-form']) }}
                        {{Form::hidden('film_id', $film->id)}}
                        {{Form::hidden('user_id', Auth::id())}}
                        <div class="row">
                            <div class="col-sm-2 text-right">{{Form::label('name', 'Your Name')}}</div>
                            <div class="col-sm-4">{{Form::text('name', null, ['required', 'class' => 'w-100'])}}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2 text-right">{{Form::label('comment', 'Comment')}}</div>
                            <div class="col-sm-4">{{Form::textarea('comment', null, ['required', 'size' => '40x3', 'class' => 'w-100'])}}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-4">{{Form::submit('Submit')}}</div>
                        </div>
                    {{ Form::close() }}
                @else   
                    Please <a href="{{ route('login') }}">login</a> or <a href="{{ route('register') }}">register</a> to comment.
                @endif
            </div>
        </div>
        
    </div>
        
@endsection