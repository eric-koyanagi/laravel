@extends('layouts.app')

@section('head')
    <script src="{{ asset('js/film-loader.js') }}" defer></script>
@endsection

@section('content')

<div class="container bg-secondary">
    <div id="filmCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
        <div id="filmList" class="carousel-inner">
            
        </div>
    
        <a class="carousel-control-prev" href="#filmCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#filmCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
    
@endsection