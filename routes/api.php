<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('films', 'FilmController@index');
Route::get('film/{film}', 'FilmController@show');
Route::post('films', 'FilmController@store');
Route::put('film/{film}', 'FilmController@update');
Route::delete('film/{film}', 'FilmController@delete');