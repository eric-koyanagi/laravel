<?php

use Illuminate\Database\Seeder;

class FilmTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        // create a test user for comments
        $testUserName = $faker->name;
        $uid = DB::table('users')->insert([
            'name' => $testUserName,
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
        ]);
         
        // create three test films with 1 comment/genre each
        for($i=0; $i < 3; $i++) {
            $fakerName = $faker->words(3, true);
            $fid = DB::table('films')->insertGetId([
                'name' => $fakerName,
                'description' => $faker->sentence(15),
                'release_date' => $faker->date(),
                'rating' => rand(1,5),
                'ticket_price' => $faker->randomFloat(2, 1, 25),
                'country' => str_random(10),
                'photo' => 'https://picsum.photos/600/400?r=' . rand(1,1000),
                'slug' => str_slug($fakerName)
            ]);
                         
            DB::table('comments')->insert([
                'comment' => $faker->sentence(5),                
                'film_id' => $fid,
                'user_id' => $uid,
                'name'    => $testUserName
            ]);
            
            DB::table('genres')->insert([
                'name' => $faker->words(2, true),
                'film_id' => $fid               
            ]);
            
        }            
    }
}