<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Film;
Use App\Comment;

class FilmController extends Controller
{
    // film details page
    public function details(Film $film)
    {    
        $film->load('comments', 'genres');
        return view('film/detail')->with('film', $film); 
    }
    
    // form to create a film
    public function create(Request $request)
    {
        return view('film/create');
    }
    
    // form action to create a film
    public function createPost(Request $request)
    {
        
        // handle file upload, moving, and saving the URL
        $file = $request->file('photo');
        $filename = 'photo-' . time() . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs('public/photos', $filename);        
        $requestAr = $request->all();
        $requestAr['photo'] = url('/storage/photos/'.$filename);
        
        // create the film in the database and redirect to its detail page
        $film = Film::create($requestAr);
        return redirect('film/'.$film->slug); 
    }
    
    // for a more detailed app, this would belong in its own controller
    public function addComment(Request $request)
    {
        Comment::create($request->all());
    }
    
    public function index(Request $request)
    {
        if($request->ajax()) {
            return (String) view('film/film-carousel-ajax')->with('films', Film::all())->render();    
        }        
        else {
            return Film::all();
        }
    }
 
    public function show(Film $film)
    {        
        return $film;
    }

    public function store(Request $request)
    {
        $film = Film::create($request->all());
        return response()->json($film, 201);
    }

    public function update(Request $request, Film $film)
    {
        $film->update($request->all());
        return response()->json($film, 200);
    }

    public function delete(Request $request, Film $film)
    {
        $film->delete();
        return response()->json(null, 204);
    }
}
