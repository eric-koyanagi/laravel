<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    public $timestamps = false;
    protected $guarded = array();    
    
    public function comments() {
        return $this->hasMany('App\Comment');
    }
    
    public function genres() {
        return $this->hasMany('App\Genre');
    }
    
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
